package com.example.aksha.module_alert;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

public class showContacts extends AppCompatActivity {

    String dbName, tableName, TAG = "showContacts__";
    SQLiteDatabase database;
    ArrayList<String> nameList, numList;
    RecyclerView view;
    LinearLayoutManager layoutManager;
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_contacts);

        DatabaseDetails databaseDetails = new DatabaseDetails();
        dbName = databaseDetails.getDBName();
        tableName = databaseDetails.getTableName();

        nameList = new ArrayList<>();
        numList = new ArrayList<>();

        Log.d(TAG, "DB: " + dbName + " TABLE: " + tableName);

        view = findViewById(R.id.contacts);
        view.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(showContacts.this);
        view.setLayoutManager(layoutManager);
        view.setItemAnimator(new DefaultItemAnimator());

        database = openOrCreateDatabase(dbName, MODE_PRIVATE, null);

        try {
            //tableContacts
            Cursor cursor = database.rawQuery("select * from '" + tableName + "';", null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    String name = cursor.getString(cursor.getColumnIndex("name"));
                    String number = cursor.getString(cursor.getColumnIndex("number"));

                    numList.add(number);
                    nameList.add(name);
                }
            }
            cursor.close();

            adapter = new Adapter(showContacts.this, nameList, numList);
            view.setAdapter(adapter);

            Log.d(TAG, "Number List: " + numList);
        } catch (Exception e) {
            Log.d(TAG, String.valueOf(e));
        }

    }
}
