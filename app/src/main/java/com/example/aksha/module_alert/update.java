package com.example.aksha.module_alert;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Objects;

public class update extends AppCompatActivity {

    int index;
    SQLiteDatabase database;
    Button save, show;
    ImageButton pickContact;
    EditText names, numbers;
    Uri contactData;

    String dbName, tableName, colName, colNumber;
    String pickedName, pickedNumber, pickedContactNumber, contactID, hasPhone, toDelete;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        DatabaseDetails databaseDetails = new DatabaseDetails();
        dbName = databaseDetails.getDBName();
        tableName = databaseDetails.getTableName();
        colName = databaseDetails.getColumnName();
        colNumber = databaseDetails.getColumnNumber();

        Intent intent = getIntent();
        index = intent.getIntExtra("index", 0);
        toDelete = intent.getStringExtra("name");

        Log.d("update__", "index: " + index + " toDelete: " + toDelete);

        save = findViewById(R.id.save);
        show = findViewById(R.id.button);
        pickContact = findViewById(R.id.pickContact);

        names = findViewById(R.id.name);
        numbers = findViewById(R.id.number);

        database = openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //tableContacts
                    Cursor cursor = database.rawQuery("select * from '" + tableName + "';", null);

                    if (cursor.getCount() > 0) {
                        while (cursor.moveToNext()) {
                            String name = cursor.getString(cursor.getColumnIndex("name"));
                            String number = cursor.getString(cursor.getColumnIndex("number"));
                            String index = cursor.getString(cursor.getColumnIndex("flag"));

                            Log.d("update__", name + " " + number + " " + index);
                        }
                    }
                    cursor.close();
                } catch (Exception e) {
                    Log.d("update__", "Show exception: " + String.valueOf(e));
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String name_ = String.valueOf(names.getText());
                    String number_ = String.valueOf(numbers.getText());
                    int flag__ = index + 1;
                    Log.d("update__", "updated name & number: " + name_ + " " + number_);

                    /*database.execSQL("update '" + tableName + "' set '" + colName + "'='" + name + "' and '" + colNumber + "'='" + number + "' " +
                            "where flag=" + index+1 + ";");*/

                    database.execSQL("update tableContacts set name ='" + name_ + "',number='" + number_ + "' where flag=" + flag__ + ";");

                    Toast.makeText(update.this, "Updated Successfully", Toast.LENGTH_SHORT).show();

                    Intent intent1 = new Intent(update.this, sendMessage.class);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                } catch (Exception e) {
                    Log.d("update__", "inside save..Exception: " + e);
                }
            }
        });

        pickContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent1, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        contactData = data.getData();
                    }
                    Cursor cursor = getContentResolver()
                            .query(contactData, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {
                        pickedName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    }

                    if (hasPhone.equals("1")) hasPhone = "true";
                    else
                        hasPhone = "false";

                    if (java.lang.Boolean.parseBoolean(hasPhone)) {
                        Cursor cursor1 = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactID, null, null);
                        while (Objects.requireNonNull(cursor1).moveToNext()) {
                            pickedNumber = cursor1.getString(cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        }
                        cursor1.close();
                    }
                    Objects.requireNonNull(cursor).close();

                    if (pickedNumber.length() == 10) {
                        pickedContactNumber = pickedNumber;
                    } else if (pickedNumber.length() == 11) {
                        pickedContactNumber = pickedNumber.substring(1);
                    } else if (pickedNumber.length() == 12) {
                        pickedContactNumber = pickedNumber.substring(2);
                    } else if (pickedNumber.length() == 13) {
                        pickedContactNumber = pickedNumber.substring(3);
                    }
                    numbers.setText(pickedContactNumber);
                    names.setText(pickedName);
                }
            }
        } catch (Exception e) {
            Log.d("update__", "Pick contact...Exception: " + e);
        }
    }
}
