package com.example.aksha.module_alert;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {

    String TAG = "RecyclerItemTouchHelper";
    private RecyclerItemTouchHelperListener listener;

    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        //foreground
        Log.d(TAG, "inside onSelectedChanged()");
        if (viewHolder != null) {
            final View view = ((Adapter.MyViewHolder) viewHolder).foreGround;
            getDefaultUIUtil().onSelected(view);
        }
    }

    @Override
    public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        //foreground
        Log.d(TAG, "inside onChildDrawOver()");
        Log.d(TAG, "inside onChildDrawOver().... dX,dY: " + dX + " " + dY);
        final View view = ((Adapter.MyViewHolder) viewHolder).foreGround;
        getDefaultUIUtil().onDrawOver(c, recyclerView, view, dX / 3, dY / 3, actionState, isCurrentlyActive);
    }

    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        Log.d(TAG, "inside clearView()");
        final View view = ((Adapter.MyViewHolder) viewHolder).foreGround;
        getDefaultUIUtil().clearView(view);
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        Log.d(TAG, "inside onChildDraw()");
        Log.d(TAG, "inside onChildDraw().... dX,dY: " + dX + " " + dY);
        final View view = ((Adapter.MyViewHolder) viewHolder).foreGround;
        getDefaultUIUtil().onDraw(c, recyclerView, view, dX / 3, dY / 3, actionState, isCurrentlyActive);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        Log.d(TAG, "inside onSwiped()");
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    //Interface . . .
    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
