package com.example.aksha.module_alert;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@SuppressWarnings("ALL")
public class sendMessage extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int PLAY_SERVICE_RESOLUTION_REQUEST = 7172;

    GPS_tracker gpsTracker;

    double latitude;
    double longitude;

    Button alert, callAmbulance, checkConnection;
    String responseString;

    boolean mRequestingLocationUpdates = false;

    OkHttpClient client;
    HttpUrl.Builder urlBuilder;

    Response response;
    Request request;

    String dbName, tableName, TAG = "sendMessage__";

    SQLiteDatabase database;

    ArrayList<String> numList;

    String mainURL = "http://api.msg91.com/api/sendhttp.php?";
    String auth_key = "233436AcdEJOX35b7f9b31";

    //The GOOGLE thing
    LocationRequest locationRequest;
    GoogleApiClient googleApiClient;
    Location location, mLastLocation;
    LocationManager manager;

    int updateInterval = 5000;//in sec...
    int fastestInterval = 3000;//in sec...
    int displacement = 10; //in metres...
    //ImageButton settingButton;

    AlertDialog infoDialog;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.updateContacts:
                Intent intent1 = new Intent(sendMessage.this, SettingActivity.class);
                intent1.putExtra("dbName", dbName);
                intent1.putExtra("tableName", tableName);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.about:
                Intent intent2 = new Intent(sendMessage.this, aboutActivitty.class);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.showContacts:
                Intent intent3 = new Intent(sendMessage.this, showContacts.class);
                startActivity(intent3);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        infoDialog = new AlertDialog.Builder(this).create();

        boolean isFirstRunHint = getSharedPreferences("PREFERENCE_HINT", MODE_PRIVATE)
                .getBoolean("isFirstRunHint", true);

        if (isFirstRunHint) {
            Log.d("isFirstRunHint", "Loading for the first time. . .");
            //show Dialog for just few secs...
            showInfoDialog();
            getSharedPreferences("PREFERENCE_HINT", MODE_PRIVATE).edit()
                    .putBoolean("isFirstRunHint", false).apply();
        } else {
            Log.d("isFirstRunHint", "Loading for the second time. . .");
        }

        manager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        boolean isEnabledGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isEnabledNetwork = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isEnabledGPS && !isEnabledNetwork) {
            showSettingAlert();
        }

        if (checkPlayServices()) {
            buildGoogleApiClient();
            createLocationRequest();
        }

        try {
            DatabaseDetails databaseDetails = new DatabaseDetails();
            dbName = databaseDetails.getDBName();
            tableName = databaseDetails.getTableName();

            callAmbulance = findViewById(R.id.callAmbulance);
            alert = findViewById(R.id.alert);
            checkConnection = findViewById(R.id.checkConnection);
            gpsTracker = new GPS_tracker(sendMessage.this);
            numList = new ArrayList<>();

            Log.d("sendMessage__", "DB: " + dbName + " table: " + tableName);

            database = openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);

            try {
                //tableContacts
                Cursor cursor = database.rawQuery("select * from '" + tableName + "';", null);

                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        String name = cursor.getString(cursor.getColumnIndex("name"));
                        String number = cursor.getString(cursor.getColumnIndex("number"));
                        String index = cursor.getString(cursor.getColumnIndex("flag"));

                        numList.add(number);
                    }
                }
                cursor.close();
                Log.d(TAG, "Number List: " + numList);
            } catch (Exception e) {
                Log.d(TAG, String.valueOf(e));
            }


            Log.d(TAG, "dbName: " + dbName + " tableName: " + tableName);

            /*if (gpsTracker.canGetLocation()) {
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
            } else {
                gpsTracker.showSettingAlert();
            }

            Log.d("Location", "latitude: " + latitude + " Longitude: " + longitude);*/

            callAmbulance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", "108", null));
                    startActivity(intent);
                }
            });

            alert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayLocation();
                    if (isNetworkAvailable()) {
                        //send SMS
                        sendSMS();
                        //send message using API
                        SendMsg msg = new SendMsg();
                        msg.execute();
                    } else {
                        //send SMS
                        sendSMS();
                    }
                }
            });

            checkConnection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean var = isNetworkAvailable();
                    if (var == true) {
                        Toast.makeText(sendMessage.this, "Internet Connected", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(sendMessage.this, "Internet Not Connected", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } catch (Exception e) {
            Log.d(TAG, "onCreate()...Exception: " + e);
        }
    }

    private void showInfoDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.info_dialog_layout, null);
        infoDialog.setView(promptView);
        infoDialog.setCancelable(true);
        infoDialog.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                infoDialog.dismiss();
                t.cancel();
            }
        }, 4000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }


    private void sendSMS() {
        String msg_1 = "http://maps.google.com/?q=" + latitude + "," + longitude;
        String msg_2 = "Help! I need you urgently at this location.";
        String msg_f = msg_2 + " " + msg_1;
        String sent = "Message Sent", delievered = "Message Delievered";

        PendingIntent sendPI = PendingIntent.getBroadcast(sendMessage.this, 0, new Intent(sent), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(sendMessage.this, 0, new Intent(delievered), 0);

        SmsManager manager = SmsManager.getDefault();

        manager.sendTextMessage(numList.get(0), null, msg_f, sendPI, deliveredPI);
        manager.sendTextMessage(numList.get(1), null, msg_f, sendPI, deliveredPI);
        manager.sendTextMessage(numList.get(2), null, msg_f, sendPI, deliveredPI);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        } else {
            return false;
        }
    }

    void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Start GPS");

        alertDialog.setMessage("GPS not enabled!!!");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alertDialog.show();
    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            //Toast.makeText(this, "Latitude: " + latitude + " Longitude: " + longitude, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("RestrictedApi")
    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(updateInterval);
        locationRequest.setFastestInterval(fastestInterval);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(displacement);
    }

    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).
                addApi(LocationServices.API).build();

        googleApiClient.connect();
        createLocationRequest();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICE_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this, "Not Supported on this device", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }


    @SuppressLint("StaticFieldLeak")
    private class SendMsg extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {

                String msg_1 = "http://maps.google.com/?q=" + latitude + "," + longitude;
                String msg_2 = "Help! I need you urgently at this location.";
                String msg_f = msg_2 + " " + msg_1;

                client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build();

                String url = mainURL + "authkey=" + auth_key +
                        "&mobiles=" + numList.get(0) + "," + numList.get(1) + "," + numList.get(2) +
                        "&message=" + msg_f +
                        "&route=" + "4" +
                        "&sender=" + "TESTIN";

                urlBuilder = Objects.requireNonNull(HttpUrl.parse(url)).newBuilder();

                String f_url = urlBuilder.build().toString();

                request = new Request.Builder().url(f_url).build();
                Log.d(TAG, "sendMsg do in background(). . .URL  " + f_url);
            } catch (Exception e) {
                Log.d(TAG, "sendMsg do in background(). . .3  " + String.valueOf(e));
            }
            try {
                response = client.newCall(request).execute();
            } catch (Exception e) {
                Log.d(TAG, "sendMsg do in background(). . .1  " + String.valueOf(e));
            }

            if (response.body() != null) {
                try {
                    responseString = response.body().string();
                } catch (IOException e) {
                    Log.d(TAG, "sendMsg do in background(). . .2  " + String.valueOf(e));
                }
            }
            Log.d(TAG, "Response String: " + responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
