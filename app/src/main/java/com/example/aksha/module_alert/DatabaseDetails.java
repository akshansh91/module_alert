package com.example.aksha.module_alert;

public class DatabaseDetails {

    //DB_Name = "myDB" ,tableName = "tableContacts"

    public String getDBName() {
        return "myDB";
    }

    public String getTableName() {
        return "tableContacts";
    }

    public String getColumnName() {
        return "name";
    }

    public String getColumnNumber() {
        return "number";
    }

    public String getColumnFlag() {
        return "flag";
    }
}
