package com.example.aksha.module_alert;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class SettingActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    RecyclerView contactList;
    RecyclerView.LayoutManager layoutManager;

    Adapter adapter;
    SQLiteDatabase database;

    String dbName, tableName;

    Button add;

    ArrayList<String> names, numbers;

    int deletedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        names = new ArrayList<>();
        numbers = new ArrayList<>();
        add = findViewById(R.id.add);

        DatabaseDetails databaseDetails = new DatabaseDetails();
        dbName = databaseDetails.getDBName();
        tableName = databaseDetails.getTableName();

        Log.d("SettingAct_", "DB_Name: " + dbName + " table: " + tableName);

        try {
            contactList = findViewById(R.id.contactList);
            contactList.setHasFixedSize(true);

            layoutManager = new LinearLayoutManager(SettingActivity.this);
            contactList.setLayoutManager(layoutManager);
            contactList.setItemAnimator(new DefaultItemAnimator());

            //Setting for item swipe listeners . . .

            ItemTouchHelper.SimpleCallback callback = new RecyclerItemTouchHelper(0, ItemTouchHelper.RIGHT, this);
            new ItemTouchHelper(callback).attachToRecyclerView(contactList);

            database = openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);

        } catch (Exception e) {
            Log.d("SettingAct_", "E_1: " + String.valueOf(e));
        }
        try {
            //Cursor cursor = database.rawQuery("select * from '" + tableName + "';", null);
            Cursor cursor = database.rawQuery("select * from '" + tableName + "';", null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    String name = cursor.getString(cursor.getColumnIndex("name"));
                    names.add(name);
                    String number = cursor.getString(cursor.getColumnIndex("number"));
                    numbers.add(number);
                }
            }
            cursor.close();

            adapter = new Adapter(SettingActivity.this, names, numbers);
            contactList.setAdapter(adapter);

            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SettingActivity.this, update.class);
                    intent.putExtra("index", deletedIndex);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

        } catch (Exception e) {
            Log.d("SettingAct_", "E_2: " + String.valueOf(e));
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        //update
        if (viewHolder instanceof Adapter.MyViewHolder) {
            String name = names.get(viewHolder.getAdapterPosition());

            // backup of removed item for undo purpose
            final int updateIndex = viewHolder.getAdapterPosition();

            Toast.makeText(SettingActivity.this, "Item to be updated: " + name, Toast.LENGTH_LONG).show();

            Log.d("OnSwipe__", "Update......Name: " + name + "Index: " + updateIndex);

            Intent intent = new Intent(SettingActivity.this, update.class);
            intent.putExtra("index", updateIndex);
            intent.putExtra("name", name);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }
}
