package com.example.aksha.module_alert;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    Button save, delete;
    EditText num_1, num_2, num_3, name_1, name_2, name_3;
    ImageButton pick_1, pick_2, pick_3;
    int PICK_CONTACT_REQUEST_1 = 1, PICK_CONTACT_REQUEST_2 = 2, PICK_CONTACT_REQUEST_3 = 3;
    //Uri contactData;

    String DB_Name, tableName;

    SQLiteDatabase database;

    LocationManager manager;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == PICK_CONTACT_REQUEST_1) {

            Uri contactData = null;
            String name = null, number = null, hasPhone = null, contactID = null, pickedContactNumber = null;

            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    contactData = data.getData();
                }
                Cursor cursor = null;
                if (contactData != null) {
                    cursor = getContentResolver()
                            .query(contactData, null, null, null, null);
                }

                if (cursor != null && cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                }

                if (hasPhone != null) {
                    if (hasPhone.equals("1")) hasPhone = "true";
                    else
                        hasPhone = "false";
                }

                if (java.lang.Boolean.parseBoolean(hasPhone)) {
                    Cursor cursor1 = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactID, null, null);
                    while (Objects.requireNonNull(cursor1).moveToNext()) {
                        number = cursor1.getString(cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    cursor1.close();
                }
                Objects.requireNonNull(cursor).close();

                if (number != null) {
                    if (number.length() == 10) {
                        pickedContactNumber = number;
                    } else if (number.length() == 11) {
                        pickedContactNumber = number.substring(1);
                    } else if (number.length() == 12) {
                        pickedContactNumber = number.substring(2);
                    } else if (number.length() == 13) {
                        pickedContactNumber = number.substring(3);
                    }
                }
                num_1.setText(pickedContactNumber);
                name_1.setText(name);
            }
        }
        if (requestCode == PICK_CONTACT_REQUEST_2) {

            Uri contactData = null;
            String name = null, number = null, hasPhone = null, contactID = null, pickedContactNumber = null;

            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    contactData = data.getData();
                }
                Cursor cursor = null;
                if (contactData != null) {
                    cursor = getContentResolver()
                            .query(contactData, null, null, null, null);
                }

                if (cursor != null && cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                }

                if (hasPhone != null) {
                    if (hasPhone.equals("1")) hasPhone = "true";
                    else
                        hasPhone = "false";
                }

                if (java.lang.Boolean.parseBoolean(hasPhone)) {
                    Cursor cursor1 = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactID, null, null);
                    while (Objects.requireNonNull(cursor1).moveToNext()) {
                        number = cursor1.getString(cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    cursor1.close();
                }
                Objects.requireNonNull(cursor).close();

                if (number != null) {
                    if (number.length() == 10) {
                        pickedContactNumber = number;
                    } else if (number.length() == 11) {
                        pickedContactNumber = number.substring(1);
                    } else if (number.length() == 12) {
                        pickedContactNumber = number.substring(2);
                    } else if (number.length() == 13) {
                        pickedContactNumber = number.substring(3);
                    }
                }

                num_2.setText(pickedContactNumber);
                name_2.setText(name);
            }
        }
        if (requestCode == PICK_CONTACT_REQUEST_3) {

            Uri contactData = null;
            String name = null, number = null, hasPhone = null, contactID = null, pickedContactNumber = null;

            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    contactData = data.getData();
                }
                Cursor cursor = null;
                if (contactData != null) {
                    cursor = getContentResolver()
                            .query(contactData, null, null, null, null);
                }

                if (cursor != null && cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                }

                if (hasPhone != null) {
                    if (hasPhone.equals("1")) hasPhone = "true";
                    else
                        hasPhone = "false";
                }

                if (java.lang.Boolean.parseBoolean(hasPhone)) {
                    Cursor cursor1 = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactID, null, null);
                    while (Objects.requireNonNull(cursor1).moveToNext()) {
                        number = cursor1.getString(cursor1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    }
                    cursor1.close();
                }
                Objects.requireNonNull(cursor).close();

                if (number != null) {
                    if (number.length() == 10) {
                        pickedContactNumber = number;
                    } else if (number.length() == 11) {
                        pickedContactNumber = number.substring(1);
                    } else if (number.length() == 12) {
                        pickedContactNumber = number.substring(2);
                    } else if (number.length() == 13) {
                        pickedContactNumber = number.substring(3);
                    }
                }

                num_3.setText(pickedContactNumber);
                name_3.setText(name);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        boolean isEnabledGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isEnabledNetwork = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isEnabledGPS && !isEnabledNetwork) {
            showSettingAlert();
        }

        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (isFirstRun) {
            Log.d("First_", "Loading for the first time. . .");
            getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                    .putBoolean("isFirstRun", false).apply();
        } else {
            Log.d("First_", "Loading for the second time. . .");
            Intent intent = new Intent(MainActivity.this, sendMessage.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        DatabaseDetails databaseDetails = new DatabaseDetails();
        DB_Name = databaseDetails.getDBName();
        tableName = databaseDetails.getTableName();

        Log.d("MainActivity_", "DB: " + DB_Name + " tableName: " + tableName);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.SEND_SMS,
                                Manifest.permission.CALL_PHONE}, 1);
            }
        }
        save = findViewById(R.id.save);
        delete = findViewById(R.id.delete);

        num_1 = findViewById(R.id.contact1);
        num_2 = findViewById(R.id.contact2);
        num_3 = findViewById(R.id.contact3);

        name_1 = findViewById(R.id.name1);
        name_2 = findViewById(R.id.name2);
        name_3 = findViewById(R.id.name3);

        pick_1 = findViewById(R.id.pickContact_1);
        pick_2 = findViewById(R.id.pickContact_2);
        pick_3 = findViewById(R.id.pickContact_3);

        database = openOrCreateDatabase(DB_Name, Context.MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS " + tableName + "(flag INTEGER, name VARCHAR, number VARCHAR)");

        pick_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(pickContact, PICK_CONTACT_REQUEST_1);
            }
        });

        pick_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(pickContact, PICK_CONTACT_REQUEST_2);
            }
        });

        pick_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(pickContact, PICK_CONTACT_REQUEST_3);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String c_1, c_2, c_3, n_1, n_2, n_3;

                c_1 = num_1.getText().toString();
                c_2 = num_2.getText().toString();
                c_3 = num_3.getText().toString();
                n_1 = name_1.getText().toString();
                n_2 = name_2.getText().toString();
                n_3 = name_3.getText().toString();

                if (c_1.length() != 10 || c_2.length() != 10 || c_3.length() != 10) {
                    Toast.makeText(MainActivity.this, "Enter 10 digit numeric value", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        database.execSQL("INSERT INTO " + tableName + " VALUES('1','" + n_1 + "','" + c_1 + "');");
                        database.execSQL("INSERT INTO " + tableName + " VALUES('2','" + n_2 + "','" + c_2 + "');");
                        database.execSQL("INSERT INTO " + tableName + " VALUES('3','" + n_3 + "','" + c_3 + "');");

                        Intent i = new Intent(MainActivity.this, sendMessage.class);
                        i.putExtra("dbName", DB_Name);
                        i.putExtra("tableName", tableName);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } catch (Exception e) {
                        Log.d("MainActivity__", "Insert Into: " + e);
                    }
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.execSQL("delete from '" + tableName + "';");
            }
        });
    }

    private void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Start GPS");

        alertDialog.setMessage("GPS not enabled!!!");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 1) {
            boolean isPerpermissionForAllGranted = false;
            if (grantResults.length > 0 && permissions.length == grantResults.length) {
                for (int i = 0; i < permissions.length; i++) {
                    isPerpermissionForAllGranted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
                }
            } else {
                isPerpermissionForAllGranted = true;
            }
            if (isPerpermissionForAllGranted) {
                Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
