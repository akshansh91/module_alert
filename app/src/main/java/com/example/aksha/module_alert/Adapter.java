package com.example.aksha.module_alert;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    Context myContext;
    ArrayList<String> name, number;

    public Adapter(Context context, ArrayList<String> name, ArrayList<String> number) {
        this.myContext = context;
        this.name = name;
        this.number = number;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.contact_list_row, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.numberContact.setText(number.get(i));
        myViewHolder.nameContact.setText(name.get(i));
    }

   /* public void removeItem(int position) {
        //cartList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        name.remove(position);
        number.remove(position);
        notifyItemRemoved(position);
    }*/

    /*public void restoreItem(ClipData.Item item, int position) {
        //cartList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }*/

    @Override
    public int getItemCount() {
        return name.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nameContact, numberContact;
        CardView foreGround;
        LinearLayout background;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nameContact = itemView.findViewById(R.id.name);
            numberContact = itemView.findViewById(R.id.number);
            foreGround = itemView.findViewById(R.id.view_foreground);
            background = itemView.findViewById(R.id.view_background);
        }
    }
}
